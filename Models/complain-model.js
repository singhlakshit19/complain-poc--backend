const mongoose = require('mongoose');

const Schema =mongoose.Schema;

const complainSchema = new Schema(
    {
        from: { type: String },
        category: { type: String },
        facility: { type: String },
        priority: { type: String },
        vendors: { type: String },
        description: { type: String }
}
);

module.exports = mongoose.model('Complain',complainSchema);