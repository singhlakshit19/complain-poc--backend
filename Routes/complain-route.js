const express =require('express');

const complainController = require('../Controllers/complain-controller');

const route = express.Router();

route.post('/add-complain', complainController.addComplain);

module.exports = route;